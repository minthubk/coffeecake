package engine;

import com.badlogic.gdx.utils.OrderedMap;

public interface IPlayerScore {
	
	public void read(OrderedMap<String, Object> object);
	public int getScore();
	public String getName();
	public int getTime();
	public int getLvl();
	public int getMaxComboTime();
}
