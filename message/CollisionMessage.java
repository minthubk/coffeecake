package engine.message;

public class CollisionMessage extends BaseMessage {

	int otherEntityId;
	boolean sensor;
	
	public boolean isSensor() {
		return sensor;
	}

	public CollisionMessage(int destinationEntityID, MessageType messageType,int otherId,boolean sensor) {
		super(destinationEntityID, messageType);
		this.sensor = sensor;
		otherEntityId = otherId;
	}

	public int getOtherEntityId() {
		return otherEntityId;
	}

}
