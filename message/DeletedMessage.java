package engine.message;

public class DeletedMessage extends BaseMessage{

	private int id;

	public DeletedMessage(int id) {
		super(MessageType.Delete);
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
