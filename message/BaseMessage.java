package engine.message;


public class BaseMessage {
	
	public int destEntityID;
    public IMessageType messageType;

    /**
     * Message to send to a particular unit
     * @param destinationEntityID
     * @param messageType
     */
    protected BaseMessage( int destinationEntityID, IMessageType messageType )
    {
        this.destEntityID = destinationEntityID;
        this.messageType = messageType;
    }

    /**
     * No target message : will be send recursively to every units in the SceneController
     * @param messageType
     */
    protected BaseMessage(IMessageType messageType)
    {
    	this(-1, messageType);
    }
}