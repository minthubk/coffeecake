package engine.behavior;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad.TouchpadStyle;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import engine.defines.EngineTextures;
import engine.defines.EngineValues;
import engine.entity.Hero;
import engine.input.TouchPadMoveMsg;
import engine.message.BaseMessage;

public class TouchpadGUI extends LayoutBehavior{
	Hero hero;
	private Touchpad pad;
	private TextButton b;
	public TouchpadGUI(Hero h){
		this.hero = h;
	}
	
	@Override
	public boolean handleMessage(BaseMessage msg) {
		float w = EngineValues.screenw/2, h = EngineValues.screenh/2;

		if(msg instanceof TouchPadMoveMsg){
			TouchPadMoveMsg m = (TouchPadMoveMsg) msg;
			
			Vector2 p = new Vector2(m.x, m.y);
			p.x -= w;
			p.y = h-p.y;
			p.x -= layout.getWidth()/2;
			p.y -= layout.getHeight()/2;
			layout.setPosition(p.x, p.y);	
		}
		return super.handleMessage(msg);
	}

	@Override
	protected void initGUI() {
//		NinePatchDrawable up = new NinePatchDrawable(mSimController.getSimulationRenderer().mAssets.getNinePatch(EngineTextures.defaultBtnUp));
//		NinePatchDrawable down = new NinePatchDrawable(mSimController.getSimulationRenderer().mAssets.getNinePatch(EngineTextures.defaultBtnDown));
//		BitmapFont fnt = mSimController.getSimulationRenderer().mAssets.getFont(EngineValues.font);
//		TextButtonStyle st = new TextButton.TextButtonStyle(up,down,up);
//		fnt.setScale(0.5f);
//		st.font = fnt;
//		b = new TextButton("Send", st);

		float w = EngineValues.screenw, h = EngineValues.screenh;
		NinePatchDrawable d = new NinePatchDrawable(mSimController.getSimulationRenderer().mAssets.getNinePatch(EngineTextures.knob));
		NinePatchDrawable k = new NinePatchDrawable(mSimController.getSimulationRenderer().mAssets.getNinePatch(EngineTextures.knob));
		pad = new Touchpad(0.5f, new TouchpadStyle(d,k));
		//layout.add(pad).width(50).height(50);
		layout.setPosition(-w/2+w/8+40, -h/2+h/8+40);
		layout.add(pad).width(h/4).height(h/4);
		Table.drawDebug(mSimController.getSimulationRenderer().getStage());
	}

	@Override
	protected void updateGUI(float dt) {
		float x = pad.getKnobPercentX();
		float y = pad.getKnobPercentY();
		Vector2 d = new Vector2(x,y);
		
		d.x = x;
		d.y = y;
		
		if(d.len() > 0.1f){
			hero.aimAt(d);
		}
		
		if(d.len() < 0.6f){
			d.x = 0;
			d.y = 0;
		}
		hero.setDir(d);

	}

}
