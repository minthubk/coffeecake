package engine.behavior;

import com.badlogic.gdx.math.Vector2;

import engine.defines.EngineValues;

public class FollowCam extends Behavior{

	float worldW;
	float worldH;
	
	public FollowCam(float maxW,float maxH){
		worldW = maxW;
		worldH = maxH;		
	}
	
	@Override
	public void update(float dTime) {
		float w = EngineValues.viewportW;
		float h = EngineValues.viewportH;
		Vector2 pos = mEntity.getBody().getPosition().cpy();
		pos.mul(mEntity.getScaleFactor());
		if(worldW > w){
			if(pos.x+w/2 > worldW+EngineValues.menuH/4){
				pos.x = worldW-w/2+EngineValues.menuH/4;
			}
			if(pos.x-w/2 < 0-EngineValues.menuH/2){
				pos.x = w/2-EngineValues.menuH/2;
			}
		}else{
			pos.x = worldW/2;
		}
		if(worldH > h){
			if(pos.y+h/2 > worldH){
				pos.y = worldH-h/2;
			}
			if(pos.y-h/2 < 0){
				pos.y = h/2;	
			}
		}else{
			pos.y = h/2;
		}
		mSimController.getSimulationRenderer().setCamPos(pos.x-EngineValues.menuH/2, pos.y);
	}
}


