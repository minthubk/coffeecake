package engine.behavior;

import java.io.FileNotFoundException;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import engine.SimulationRenderer.Layer;

public class TextureRenderer extends ImageBehavior {
	protected Image image= new Image();
	private Layer layer = Layer.ActorBG;
	
	protected void initImage() {
		//setTexture(Textures.def);
	}
	
	protected void updateImage(float dt) {
		image.setScale(mScale.x,mScale.y);
		image.setRotation(mEntity.getAngle()*MathUtils.radDeg);// /!\ les appel a box2D coutent cher, il faudrait faire un systeme de coord pour cacher le moteur physique
		Vector2 p = mEntity.getBody().getPosition().cpy();
		p.mul(mEntity.getScaleFactor());
		float a = mEntity.getBody().getAngle();
		image.setRotation(a*MathUtils.radDeg);
		float w = image.getWidth()/2;
		float h = image.getHeight()/2;
		image.setPosition(p.x-w, p.y-h);

	}
	
	@Override
	public void dispose() {
		if(image != null) {
			image.remove();
		}
	};
	
	public void setColor(Color color) {
		image.setColor(color);
	}
	
	public Color getColor() {
		return image.getColor();
	}
	
	public void setTexture(String textureName){
		if(image != null)
			image.remove();
		TextureRegionDrawable r = null;
		try {
			r = mSimRenderer.mAssets.getTexture(textureName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		image = new Image(r);
		image.setScale(mScale.x,mScale.y);
		image.setOrigin(image.getWidth()/2,image.getHeight()/2);
		mSimRenderer.addActor(image, layer);
	}

	public Layer getLayer() {
		return layer;
	}  

	public void setLayer(Layer layer) {
		this.layer = layer;
		image.remove();
		mSimRenderer.addActor(image, layer);
	}
}

