package engine.behavior;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import engine.SimulationController;
import engine.SimulationRenderer;
import engine.entity.Entity;

public abstract class ImageBehavior extends Behavior {
	
	protected boolean drawSensor = false;
	protected SimulationRenderer mSimRenderer;
	protected Vector2 mScale = new Vector2(1,1);
	
	
	@Override
	public void init(Entity entity, SimulationController simController) {
		super.init(entity, simController);
		mSimRenderer = mSimController.getSimulationRenderer();
		initImage();
	};
	
	@Override
	public void update(float dTime) {
		updateImage(dTime);
	}
	
	protected abstract void initImage() ;
	protected abstract void updateImage(float dt) ;
	public abstract void setColor(Color color) ;
	public abstract Color getColor() ;
	public abstract void dispose();
	
	public void setDrawSensor(boolean b) {
	}

	public Vector2 getmScale() {
		return mScale;
	}

	public void setmScale(Vector2 mScale) {
		this.mScale = mScale;
	}
	
	public void setmScale(float x,float y) {
		this.mScale = new Vector2(x, y);
	}
}
