package engine.behavior;

import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;

import engine.defines.EngineTextures;
import engine.defines.EngineValues;
import engine.entity.Hero;
import engine.input.TouchFireMsg;
import engine.message.BaseMessage;
import game.entity.SushiHero;

public class ButtonsGUI extends LayoutBehavior{

	Hero hero;
	public TextButton Ab;
	public TextButton Bb;
	public boolean A,B;
	public ButtonsGUI(Hero hero){
		this.hero = hero;
	}

	@Override
	protected void initGUI() {
		float w = EngineValues.viewportW, h = EngineValues.viewportH;
		NinePatchDrawable up = new NinePatchDrawable(mSimController.getSimulationRenderer().mAssets.getNinePatch(EngineTextures.defaultBtnUp));
		NinePatchDrawable down = new NinePatchDrawable(mSimController.getSimulationRenderer().mAssets.getNinePatch(EngineTextures.defaultBtnDown));
		TextButtonStyle st = new TextButton.TextButtonStyle(up,down,up);
		BitmapFont fnt = mSimController.getSimulationRenderer().mAssets.getFont(EngineValues.font);
		fnt.setScale(0.5f);
		st.font = fnt;
		//w*-.8f+EngineValues.menuH, h*.8f); //(
		layout.setPosition(0, -h/2);
		Bb = new TextButton("",st);
		Bb.addListener(new ClickListener(){
			@Override
			public void clicked (InputEvent event, float x, float y) {
				B = true;
				A = false;
				hero.setAB(A, B);
			}
		});
		layout.add(Bb).width(w/2).height(w/4);
	}
	
	@Override
	public boolean handleMessage(BaseMessage msg) {
		if(msg instanceof TouchFireMsg){
			A = ((TouchFireMsg) msg).touched;
			hero.setAB(A, B);
		}
		return super.handleMessage(msg);
	}

	@Override
	protected void updateGUI(float dt) {
		SushiHero h = (SushiHero) hero;
		
		if(h.getSecondary() != null && h.getSecondary().canFire()){
			Bb.setVisible(true);
			Bb.getLabel().setText(h.getSecondary().getName());
		}else{
			Bb.setVisible(false);
			Bb.setChecked(false);
			B = false;
			hero.setAB(A, B);
		}
	}
}
