package engine.behavior.tools;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

import engine.SimulationController;
import engine.behavior.Behavior;
import engine.entity.Entity;

public class Acceleration extends Behavior{

	float acc = 20;
	private float a;
	private boolean set;

	@Override
	public void init(Entity entity, SimulationController simController) {
		super.init(entity, simController);
		a = mEntity.getAngle();
	}

	public void setAcc(float acc){
		this.acc =acc;
	}

	public void setAngle(float a){
		this.a = a;
		set = true;
	}

	@Override
	public void update(float dTime) {
		Vector2 v = mEntity.getBody().getLinearVelocity();
		if(!set){
			float angle = 0;
			if(Math.abs(v.x) > Math.abs(v.y)){
				if(v.x > 0)
					angle = 0;
				if(v.x < 0)
					angle = MathUtils.PI;
			}else{
				if(v.y > 0)
					angle = MathUtils.PI/2;
				if(v.y < 0)
					angle = MathUtils.PI*3/2;
			}
			a = angle;
		}
		Vector2 f = new Vector2(acc*MathUtils.cos(a),acc*MathUtils.sin(a));
		mEntity.getBody().applyForceToCenter(f);

	}



}
