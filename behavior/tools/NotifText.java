package engine.behavior.tools;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;

import engine.behavior.LayoutBehavior;
import engine.defines.EngineValues;

public class NotifText extends LayoutBehavior{
	public Label text;
	String s;
	
	public NotifText(String s){
		this.s = s;
		relative = false;
	}
	
	@Override
	protected void initGUI() {
		BitmapFont fnt = mSimController.getSimulationRenderer().mAssets.getFont(EngineValues.font);
		LabelStyle st = new LabelStyle(fnt,Color.WHITE);
		text = new Label(s, st);
		text.setAlignment(Align.center);
		if(!relative){
			layout.setTransform(true);
			layout.setScale(mSimController.getSimulationRenderer().getScale());
		}
		layout.add(text).width(32).height(32).center();
	}

	@Override
	protected void updateGUI(float dt) {
		float scale = mEntity.getScaleFactor();
		Vector2 v =mEntity.getBody().getPosition();
		layout.setPosition(v.x*scale,(v.y)*scale);
	}

}
