package engine.gui;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ButtonGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Label.LabelStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton.TextButtonStyle;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.NinePatchDrawable;
import com.badlogic.gdx.utils.ObjectMap;

import engine.InputController.InputType;
import engine.KeyNames;
import engine.SimulationController;
import engine.defines.EngineSounds;
import engine.defines.EngineTextures;
import engine.defines.EngineValues;
import engine.input.InputData;
import engine.input.KeyInputProcessor.Action;
import engine.message.PlaySound;

public class PlayerControlInfo extends Table{
	public InputData data;

	ControlGUI controler;
	SimulationController sim;
	Table binds;
	Table buttons;
	private Table options;
	private Table body;
	Label hint;
	ObjectMap<Action,Label> bindsText = new ObjectMap<Action,Label>();
	TextButton readyButton;
	TextButton reset;
	boolean ready = false;
	boolean rebind = false;
	boolean dualRot = false;
	private boolean mouseRot;
	Action focus = null;
	Label typeLabel;
	int slot = 0;
	int maxline = 10;
	TextButton def,mouse,dual;
	
	private TextButtonStyle style;
	private TextButtonStyle check;

	private LabelStyle labelstyle;

	private Label rotate;


	public void bind(int key){
		if(key < 0)
			return;
		if(rebind){
			if(data.bindKey(focus,key)){
				int id = focus.ordinal();
				id++;
				while(id < data.actions.size ){
					focus = data.actions.get(id);
					if(isNecessary(focus))
						break;
					id++;
				}
				if(id >= data.actions.size)
					focus = null;
				else
					focus = data.actions.get(id);
			}
		}
		if(focus == null)
			rebind = false;
	}

	public boolean isNecessary(Action a){
		if(!dualRot && ( a == Action.rotDown  || a == Action.rotUp || a == Action.rotLeft || a == Action.rotRight)){
			return false;
		}
		return true;
	}

	public InputData getData() {
		return data;
	}

	public boolean isReady() {
		return ready;
	}

	public boolean isRebind() {
		return rebind;
	}

	public PlayerControlInfo(InputData d,ControlGUI controler,SimulationController sim){
		this.data = d;
		this.sim = sim;
		this.controler = controler;
		createPlayerGUI();
	}

	public void setData(InputData d, int currentSlot){
		this.data = d;
		this.slot = currentSlot;
		if(data == null){
			binds.setVisible(false);
			options.setVisible(false);
			buttons.setVisible(false);
			ready = true;
		}else{
			ready = false;
			options.setVisible(true);
			binds.setVisible(true);
			buttons.setVisible(true);
			if(!controler.hasNumpad() || data.type == InputType.Numpad_and_mouse){
				def.setVisible(false);
				mouse.setVisible(false);
				dual.setVisible(false);
				mouse.setVisible(false);
				rotate.setVisible(false);
				mouse.setChecked(true);
			}
			if(data.type == InputType.Numpad_and_mouse){
				hint.setText("Fire : Mouse1 ");
			}
			setBinds();
		}
	}

	public void deleteMouseOption(){
		dual.setChecked(true);
		dual.setVisible(true);
		def.setVisible(true);
		rotate.setVisible(true);
		mouse.setChecked(false);
		mouse.remove();
	}
	
	private void updateInputs() {

		typeLabel.setText("Player " + (slot+1 ) +": "+ data.type.name());
		for (Action a : data.actions) {
			if(data.binds.containsKey(a)){
				if(isNecessary(a)){
					Label l = bindsText.get(a);
					l.setVisible(true);
					if(a == focus){
						l.setColor(Color.RED);
					}else
						l.setColor(Color.WHITE);
					String text = getActionText(a);
					l.setText(text + " : " + KeyNames.getName(data.type,data.binds.get(a)));
				}else{
					Label l = bindsText.get(a);
					l.setVisible(false);

				}
			}
		}
	}

	private String getActionText(Action a) {
		switch (a){
		case Down:
			return "Down";
		case Fire:
			return "Fire";
		case Fire2:
			return "Secondary fire";
		case Left:
			return "Left";
		case Right:
			return "Right";
		case Up:
			return "Up";
		case rotDown:
			return "aim down";
		case rotLeft:
			return "aim left";
		case rotRight:
			return "aim right";
		case rotUp:
			return "aim up";
		default:
			break;
		}
		return "";
	}

	public void createPlayerGUI(){
		NinePatchDrawable up = null,down = null;
		up = new NinePatchDrawable(sim.getSimulationRenderer().mAssets.getNinePatch(EngineTextures.defaultBtnUp));
		down = new NinePatchDrawable(sim.getSimulationRenderer().mAssets.getNinePatch(EngineTextures.defaultBtnDown));
		BitmapFont fnt = sim.getAssets().getFont(EngineValues.classicFont);
		fnt.setScale(1f);
		labelstyle = new LabelStyle(fnt,Color.WHITE);

		hint = new Label("",labelstyle);

		check = new TextButtonStyle(up,down,down);
		
		check.over = down;

		style = new TextButtonStyle(up,down,up);
		style.font = fnt;
		check.font = style.font;

		typeLabel = new Label("",labelstyle);

		body = new Table();
		body.add(createBindView());
		body.add(createOptions());
		add(typeLabel);
		row();
		add(body).colspan(4);
		row();
		add(createButtons()).colspan(4).left();

		if(data == null){
			binds.setVisible(false);
			buttons.setVisible(false);
			options.setVisible(false);
		}
	}

	private Actor createOptions() {
		options = new Table();
		ButtonGroup g = new ButtonGroup();
		g.setMaxCheckCount(1);
		mouse = new TextButton("Mouse",check);
		dual = new TextButton("Dual \nkeyset",check);
		def = new TextButton("Lock \non fire", check);
		rotate = new Label("Aiming :",labelstyle);
		rotate.setAlignment(Align.center);
		g.add(def);
		g.add(dual);
		options.add(rotate);
		options.row();
		options.add(def).width(90);
		options.row();
		options.add(dual).width(90);
		//options.add(mouse).width(90);
		return options;
	}

	public void toggleReady(){
		sim.sendMessage(new PlaySound(EngineSounds.button));
		ready = !ready;
	}

	private Actor createButtons()  {
		buttons = new Table();

		readyButton = new TextButton("Start",check);
		readyButton.addListener(new ChangeListener(){
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				toggleReady();
			}
		});
		readyButton.setChecked(ready);
		//buttons.add(readyButton).width(EngineValues.screenw/8);
		reset = new TextButton("rebind",style);
		buttons.add(reset).width(EngineValues.screenw/8);
		return buttons;
	}

	public void setBinds(){
		int i = 0;
		for(Action a : data.actions){

			Label l = new Label(""+i, labelstyle);
			bindsText.put(a,l);
			l.setVisible(false);
			if(i%2 == 0){
				binds.row();
				binds.add(l).width(EngineValues.screenw/4).left();
			}else
				binds.add(l).width(EngineValues.screenw/4).right();
			i++;
		}
		binds.row();
		binds.add(hint).colspan(1).left();
	}

	private Actor createBindView() {
		binds = new Table();
		binds.setColor(Color.GRAY);
		return binds;
	}

	public void updateGUI(float dt){
		if(mouseRot){
			hint.setVisible(true);
			hint.setText("Use mouse to aim");
		}else{
			hint.setVisible(false);
		}
		dualRot = dual.isChecked();
		mouseRot = mouse.isChecked();
		if(data != null){
			data.lockRot = def.isChecked();
			data.mouseRot = mouseRot;
		}
		readyButton.setChecked(ready);

		if(reset.isPressed() && !reset.isChecked()){
			ready = false;
			rebind = true;
			data.clearBinds();
			readyButton.setChecked(false);
			focus = data.actions.get(0);
			sim.sendMessage(new PlaySound(EngineSounds.button));
		}
		if(rebind){
			readyButton.setVisible(false);
			reset.setChecked(true);
		}else{
			readyButton.setVisible(true);
			reset.setChecked(false);
		}

		if(data != null)
			updateInputs();
		
	}
	
	public void setActive(int i){
		String t = "Player " + (i+1);
		if(!controler.hasKeyboard()){
			t+= "\n"+EngineValues.keyboardText;
		}
		if(!controler.hasNumpad()){
			t+= "\n"+EngineValues.numpadText;
		}
		typeLabel.setText(t);
		typeLabel.setVisible(true);
	}

}
