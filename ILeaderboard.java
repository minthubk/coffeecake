package engine;

import com.badlogic.gdx.utils.Array;

public interface ILeaderboard {
	int REQUEST_LEADERBOARD = 1;
	public void init(SimulationController sim);

	public void submitScore(String leaderboard, IPlayerScore player);
	public void sumbitCallback(boolean success);
	
	public void submitStat(int score,int wave,int time,int combo,int nbp);

	public void getLeaderboard(String leaderboard);
	public void leaderBoardCallback(boolean success,String data);

	public Array<IPlayerScore> getScores();

}
