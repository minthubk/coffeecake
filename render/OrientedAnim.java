package engine.render;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import engine.Assets;
import engine.entity.Entity;
import engine.entity.Entity.Orientation;

public class OrientedAnim {
	HotPoint self; //Self point of junction for texture composition, ignored by the renderer if this is the main (first) texture
	ObjectMap<Orientation,Anim> anims = new ObjectMap<Entity.Orientation, Anim>();
	Array<String> mods;
	ObjectMap<String, HotPoint> hotpoints;
	ObjectMap<String, OrientedAnim> childTextures;

	
	public OrientedAnim(Anim north, Anim south, Anim east, Anim west) {
		super();
		Vector2 v = new Vector2();
		this.self =  new HotPoint(v, v, v, v);
		anims.put(Orientation.N, north);
		anims.put(Orientation.S, south);
		anims.put(Orientation.E, east);
		anims.put(Orientation.W,west);
		childTextures = new ObjectMap<String, OrientedAnim>();
		hotpoints = new ObjectMap<String, HotPoint>();
		mods = new Array<String>();
	}
	
	public OrientedAnim(Anim north, Anim south, Anim east, Anim west,HotPoint self) {
		this(north,south,east,west);
		this.self = self;
	}
	
	public OrientedAnim(Assets a,String north, String south, String east, String west,HotPoint self) {
		this(new Anim(a, north),new Anim(a,south),new Anim(a,east),new Anim(a,west),self);
	}
	
	public OrientedAnim(OrientedAnim o ) {
		super();
		this.self = new HotPoint(o.getSelf());
		anims.put(Orientation.N, o.getAnim(Orientation.N));
		anims.put(Orientation.S, o.getAnim(Orientation.S));
		anims.put(Orientation.E, o.getAnim(Orientation.E));
		anims.put(Orientation.W,o.getAnim(Orientation.W));
		this.mods = o.mods;
		this.hotpoints = o.hotpoints;
		childTextures = new ObjectMap<String, OrientedAnim>();
	}

	public OrientedAnim(Anim anim) {
		this(anim, anim, anim, anim);
	}

	public Anim getAnim(Orientation o){
		return anims.get(o);
	}

	public Anim north(){
		return anims.get(Orientation.N);
	}

	public Anim south(){
		return anims.get(Orientation.S);
	}
	
	public Anim east(){
		return anims.get(Orientation.E);
	}
	
	public Anim west(){
		return anims.get(Orientation.W);
	}

	public void addMod(String name,HotPoint pos){
		mods.add(name);
		hotpoints.put(name, pos);
	}

	public void setTextureMod(String name,OrientedAnim child){
		if(mods.contains(name, false))
			childTextures.put(name,child);
	}

	public ObjectMap<String, OrientedAnim> getChildTextures() {
		return childTextures;
	}

	public void setChildTextures(ObjectMap<String, OrientedAnim> childTextures) {
		this.childTextures = childTextures;
	}

	public HotPoint getHotPoint(String key) {
		return hotpoints.get(key);
	}
	public HotPoint getSelf() {
		return self;
	}
	public void setSelf(HotPoint self) {
		this.self = self;
	}

	public Array<String> getMods() {
		return mods;
	}

	public void setMods(Array<String> mods) {
		this.mods = mods;
	}
}
