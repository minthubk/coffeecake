package engine.fsm.action;

public interface Action {
	public void exec();
}
