package engine.fsm.transition;

import engine.fsm.state.State;


public class TimeTransition extends Transition{
	
	float time = 15000;
	float t = time;
	
	public void setTime(float time){
		this.time = t;
		t = time;
	}
	
	public TimeTransition(){
		super();
	}
	
	public TimeTransition(float t){
		super();
		setTime(t);
	}
	
	@Override
	public boolean isVerified() {
		return t <= 0;
	}
	
	@Override
	public void update(float dTime) {
		t -= dTime;
	}
	
	@Override
	public void exec(State src, State target) {
		super.exec(src, target);
	}
}
