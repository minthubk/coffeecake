package engine.fsm.transition;

import engine.fsm.state.State;


public class AlwaysTrue extends Transition{

	@Override
	public boolean isVerified() {
		return true;
	}

	@Override
	public void exec(State src, State target) {
	}

}
