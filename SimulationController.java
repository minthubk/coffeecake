package engine;


import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net.HttpRequest;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Timer;

import engine.ad.IActivityRequestHandler;
import engine.defines.EngineValues;
import engine.entity.Entity;
import engine.entity.Particle;
import engine.gui.PlayerControlInfo;
import engine.input.InputData;
import engine.message.BaseMessage;
import engine.message.DeletedMessage;

/**
 * @author clallier
 * abstract class 
 * Its job is to represent a level (a simulation = a level)
 * Should be inited, in order to describe the level.
 * the Update and Render phases are called automatically
 */
public abstract class SimulationController { 
	protected Array<PlayerControlInfo> infos = null;
	protected ILeaderboard board;
	protected boolean launched = false;
	
	protected static int nextID = 0;
	protected static int nextPID = 0;

	protected String TAG;
	// all scene units
	protected ObjectMap<Integer, Entity> mEntities;
	protected ObjectMap<Integer, Particle> mParticules;

	protected Array<Integer> mSheduledToDeleteEntityKeys;
	protected Array<Integer> mSheduledToDeleteParticleKeys;

	protected Array<Integer> mToToggleOnEntities;
	protected Array<Integer> mToToggleOffEntities;
	protected SimulationRenderer mSimulationRenderer;
	protected IActivityRequestHandler mRequestHandler;
	static SimpleResponseListener listener = new SimpleResponseListener();
	

	// TODO map !
	protected Vector2 mSize;

	//Box 2d World
	protected World mWorld;

	// Timer
	protected Timer mTimer;

	private Array<Entity> players = new Array<Entity>();
	private InputController inputs;
	float dt;

	public Array<Entity> getPlayers() {
		return players;
	}

	public InputController getInputs() {
		return inputs;
	}
	
	public SimulationController(IActivityRequestHandler requestHandler) {
		TAG = getClass().getName();
		mEntities = new ObjectMap<Integer, Entity>();
		mSheduledToDeleteEntityKeys = new Array<Integer>();
		mToToggleOnEntities = new Array<Integer>();
		mToToggleOffEntities = new Array<Integer>();
		mRequestHandler = requestHandler;
		mParticules = new ObjectMap<Integer, Particle>();
		mSheduledToDeleteParticleKeys = new Array<Integer>();

		
		mWorld = new World(new Vector2(0, 0), true);
		mTimer = new Timer();		

//		mTimer.scheduleTask(new Task() {
//			@Override
//			public void run() {
//				System.out.println(dt +"ms | " + 1/dt*1000 + "fps");
//			}
//		}, 0, 1, -2);
	}

	public void start(String file) {
		init(file);
	}

	protected void resume() {
	}

	protected void pause() {
	}

	public Entity createPlayer(Entity entity,float i,float j,InputData d) {
		entity = createEntity(entity, i, j);
		players.add(entity);
		inputs.setEntityId(d.type,d.binds,entity.getId(),d.mouseRot);
		return entity;

	}

	public Entity createEntity(Entity entity, float i, float j) {
		if(entity != null) {
			int id = nextID++;
			entity.init(id, this, new Vector2(i, j));
			mEntities.put(id, entity);
		} 
		return entity;
	}
	/*Create entity as particle, Id are reused , delete message is not sent, particule ids should not be used by app*/
	public Particle createParticle(Particle e,Vector2 pos){
		if(e != null) {
			int pid = nextPID++;
			e.particle = true;
			e.init(pid, this, new Vector2(pos));
			mParticules.put(pid, e);
		} 
		return e;
	}
	
	public Entity createEntity(Entity entity, Vector2 p) {
		return this.createEntity(entity,p.x,p.y);
	}
	
	/**
	 * 
	 * @param type
	 * @param delay
	 * @param interval 
	 * @param repeatCount : -2 = FOREVER 
	 * @return
	 */
	public Trigger createTrigger(Trigger trigger, int delay, int interval, int repeatCount) {
		if(trigger != null) {
			trigger.init(this);
			mTimer.scheduleTask(trigger, delay, interval, repeatCount);
		}
		return trigger;
	}

	/**
	 * Permit to initialize the level on loading
	 * called by the CTor
	 * @param file
	 */
	abstract public void init(String file);


	public void load(String file){
		//no file : should build the path automatically 
	}

	public void save(String file){
		//no file : should build the path automatically : name + "save".xml
	}

	// Returns true if the entity or any components handled the message
	public boolean sendMessage( BaseMessage msg )
	{
		boolean messageHandled = false;

		// pass message to simulation renderer
		if(getSimulationRenderer() != null)
			messageHandled = getSimulationRenderer().sendMessage(msg);
		
		// pass message to targeted entity 
		if(!messageHandled && msg.destEntityID != -1) {
			Entity e = mEntities.get(msg.destEntityID);
			if(e != null)
				messageHandled = e.sendMessage(msg);
		}
		
		// broadcast to all entities
		else if(!messageHandled) //TODO should i broadcst to particule?
			for (Entity e : mEntities.values())
				messageHandled |= e.sendMessage(msg);

		return messageHandled;
	}

	public Vector2 getSize() {
		return mSize;
	}
	public void update(float dTime) {
		dt = dTime;
		// update them all
		for (Entity u : mEntities.values()) {
			if(u == null){
				System.err.println("WTF happened here?");
			}else{
				u.update(dTime);
			}
		}
		
		for (Particle u : mParticules.values()) {
			if(u == null){
				System.err.println("WTF happened here?");
			}else{
				u.update(dTime);
			}
		}
		
		// update Box2D world
		mWorld.step(dTime/1000f, 1,0);

		// delete entities
		cleanupEntities();
		cleanupParticles();
	}

	public void removeEntity(int id) {
		mSheduledToDeleteEntityKeys.add(id);
	}
	
	public void removeParticle(int id) {
		mSheduledToDeleteParticleKeys.add(id);
	}
	
	public void toggleEntity(int id,boolean t) {
		if(t){
			mToToggleOnEntities.add(id);
		}else{
			mToToggleOffEntities.add(id);
		}
	}

	

	public Entity getEntity(int id){
		return mEntities.get(id);
	}
	
	public void setInputs(InputController mInputs) {
		this.inputs = mInputs;
	}

	public void dispose() {
		for(int k :mEntities.keys())
			removeEntity(k);

		cleanupEntities();
		cleanupParticles();
		mEntities.clear();
		mSheduledToDeleteEntityKeys.clear();
		mSheduledToDeleteParticleKeys.clear();
		mToToggleOnEntities.clear();
		mToToggleOffEntities.clear();
	}
	private void cleanupEntities() {
		for(Integer i : mSheduledToDeleteEntityKeys ) {
			Entity e = null;
			Body b = null;
			e = mEntities.get(i);
			// delete body 
			if(e != null){
				e.setmDead(true);
				e.dispose();

				sendMessage(new DeletedMessage(i));
				
				b = e.getBody();
				if(b != null)
					mWorld.destroyBody(e.getBody());
				
				// delete entity
			}
			mEntities.remove(i);
		}
		for (Integer i : mToToggleOnEntities) {
			mEntities.get(i).getBody().setActive(true);
		}

		for (Integer i : mToToggleOffEntities) {
			mEntities.get(i).getBody().setActive(false);
		}
		mToToggleOffEntities.clear();
		mToToggleOnEntities.clear();
		mSheduledToDeleteEntityKeys.clear();
	}
	
	private void cleanupParticles() {
		for(Integer i : mSheduledToDeleteParticleKeys ) {
			Particle e = null;
			Body b = null;
			e = mParticules.get(i);
			// delete body 
			if(e != null){
				e.setmDead(true);
				e.dispose();
				b = e.getBody();
				if(b != null)
					mWorld.destroyBody(b);
			}
			mParticules.remove(i);
		}
		mSheduledToDeleteParticleKeys.clear();
		
	}

	public void setRenderer(SimulationRenderer simRenderer) {
		mSimulationRenderer = simRenderer;
	}

	public void resize(int width, int height) {
		
	}

	public World getWorld() {
		return mWorld;
	}

	public Assets getAssets(){
		return mSimulationRenderer.mAssets;
	}
	
	public SimulationRenderer getSimulationRenderer() {
		return mSimulationRenderer;
	}

	public void createGameScreen(Array<PlayerControlInfo> infos) {
		clearGameScreen();
		launched = true;
		this.infos = infos;		
		createGameEntities(infos);
		createGameGUI();
	}

	protected abstract void createGameEntities(Array<PlayerControlInfo> infos);

	protected void createGameGUI() {
	}

	protected void clearGameScreen() {
		getPlayers().clear();
		for (Entity e : mEntities.values()) {
			e.destroy();
		}
		for (Entity e : mParticules.values()) {
			e.destroy();
		}
	}

	public ILeaderboard getBoard() {
		return board;
	}

	public Array<PlayerControlInfo> getInfos() {
		return infos;
	}
	public void setBoard(ILeaderboard board) {
		this.board = board;
	}
	
	public static boolean isMobileDevice(){
		return Gdx.app.getType() == ApplicationType.Android || Gdx.app.getType() == ApplicationType.iOS || EngineValues.mobileDebug;
	}
	
	public static void initInternetTest(){
		HttpRequest request = new HttpRequest("HEAD");
		request.setUrl("http://www.google.com");
		request.setTimeOut(2000);
		Gdx.net.sendHttpRequest(request, listener);
	}
	
	public static boolean getInternetTest() {
		return listener.getStatus();
	}

	public void showAds(boolean isBanner) {
		mRequestHandler.show(isBanner);
	}
	
	public void hideAds() {
		mRequestHandler.hide();
	}
}
