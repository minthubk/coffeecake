package engine;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;

import engine.entity.Entity;
import engine.message.CollisionMessage;
import engine.message.MessageType;

public class ContactListenerImpl implements ContactListener{

	@Override
	public void beginContact(Contact contact) {
		if(contact.isTouching()) {
			Entity a = (Entity)contact.getFixtureA().getBody().getUserData();
			Entity b = (Entity)contact.getFixtureB().getBody().getUserData();
			if(!contact.getFixtureB().isSensor())
				sendCollisionMessage(a, b, MessageType.BeginCollision,contact.getFixtureA().isSensor());
			if(!contact.getFixtureA().isSensor())
				sendCollisionMessage(b, a, MessageType.BeginCollision,contact.getFixtureB().isSensor());
		}
	}

	@Override
	public void endContact(Contact contact) {
		Entity a = (Entity)contact.getFixtureA().getBody().getUserData();
		Entity b = (Entity)contact.getFixtureB().getBody().getUserData();
		if(!contact.getFixtureB().isSensor())
			sendCollisionMessage(a, b, MessageType.EndCollision,contact.getFixtureA().isSensor());
		if(!contact.getFixtureA().isSensor())
			sendCollisionMessage(b, a, MessageType.EndCollision,contact.getFixtureB().isSensor());
	}

	private void sendCollisionMessage(Entity entity,Entity other, MessageType message,boolean sensor) {
		if(entity != null)
			entity.sendMessage(new CollisionMessage(entity.getId(), message,other.getId(),sensor));
	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {
	}

	@Override
	public void postSolve(Contact contact, ContactImpulse impulse) {
	}

}
