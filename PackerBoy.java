package engine;

import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

import engine.defines.EngineTextures;
import engine.defines.EngineValues;

public class PackerBoy {
	
	// use TexturePacker in order to generate the texture pack
	// take a list of images and give a "game.atlas" texture atlas 
	// https://code.google.com/p/libgdx-texturepacker-gui/
	public static void packTo(String pathToAtlasAndroid) {

		if(EngineValues.texturePackingDebug) {
			String pathToTextures = "../"+EngineValues.texturesPath;
			String pathToAtlasDesktop = "bin/data/Atlas";
			TexturePacker2.process(pathToTextures, pathToAtlasDesktop, EngineTextures.atlasName);
			TexturePacker2.process(pathToTextures, pathToAtlasAndroid, EngineTextures.atlasName);
		}
	}
}
