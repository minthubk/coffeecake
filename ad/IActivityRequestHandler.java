package engine.ad;

public interface IActivityRequestHandler  {
	public void show(boolean isBanner);
	public void hide();
}
