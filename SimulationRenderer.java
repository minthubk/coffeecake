package engine;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

import engine.behavior.LayoutBehavior;
import engine.defines.EngineValues;
import engine.message.BaseMessage;


public class SimulationRenderer implements Disposable {

	public enum Layer{Background, ActorBG, Actors1, ActorFG, AbsoluteGUI,Gui};

	private SimulationController mSimController;
	public Assets mAssets;
	public SoundPlayer mPlayer;
	protected BitmapFont mFont;
	private SpriteBatch mSpriteBatch;

	private Stage stage;
	protected Skin skin;
	protected Array<Table> layers = new Array<Table>();
	Box2DDebugRenderer debugRenderer;
	Matrix4 debugMatrix;

	public SimulationRenderer(Assets assets) {
		mAssets = assets;

		setStage(new Stage());
		for (Layer l : Layer.values()) {
			Table t = new Table();
			layers.insert(l.ordinal(), t);
			t.setZIndex(l.ordinal());
			stage.addActor(t);
		} 
		layers.get(Layer.Gui.ordinal()).setTransform(true);
		layers.get(Layer.AbsoluteGUI.ordinal()).setTransform(true);
		layers.get(Layer.AbsoluteGUI.ordinal()).setPosition(0, 0);
		setCamViewport(EngineValues.viewportW, EngineValues.viewportH); 
		mSpriteBatch = new SpriteBatch();

		mFont = mAssets.getFont(EngineValues.font);
		mPlayer = new SoundPlayer(this);

		mSimController = null;

		debugRenderer=new Box2DDebugRenderer();
	}

	public void addGUIWidget(LayoutBehavior b){
		if(b.relative){
			Table t = b.layout;
			t.setTransform(true);
			Vector2 scPos = b.screenPos;
			t.setPosition(scPos.x,scPos.y);
			addActor(b.layout, Layer.Gui);
		}else{
			addActor(b.layout, Layer.ActorFG);
		}
	}

	public void	updateGUIPosition(){
		layers.get(Layer.Gui.ordinal()).setPosition(getCamPos().x,getCamPos().y);
	}

	public void addActor(Actor w, Layer layer){
		layers.get(layer.ordinal()).addActor(w);
	}

	public void setSimulation(SimulationController simController) {
		mSimController = simController;	
	}

	// begin rendering phase
	public void render(float delta) {
		updateGUIPosition();
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
		getStage().draw();
		if(EngineValues.box2DDebug)
			renderDebug();
	}

	private void renderDebug() {
		debugMatrix=new Matrix4(getStage().getCamera().combined);
		debugMatrix.scale(EngineValues.bodyScale, EngineValues.bodyScale, 1f);

		mSpriteBatch.begin();
		debugRenderer.render(mSimController.getWorld(), debugMatrix);
		mSpriteBatch.end();
	}


	public void dispose() {
		mSpriteBatch.dispose();
		debugRenderer.dispose();
		mFont.dispose();
	}

	public boolean sendMessage(BaseMessage msg) {
		return mPlayer.sendMessage(msg);
	}

	public void setCamViewport(float width, float height) {
		getStage().getCamera().viewportWidth = width;
		getStage().getCamera().viewportHeight = height; 
		getStage().getCamera().position.set(width *.5f, 0.5f, 0f);
		layers.get(Layer.Gui.ordinal()).setScale(getScale());
		getStage().getCamera().update();
	}

	public Vector2 getCamViewport() {
		return(new Vector2(getStage().getCamera().viewportWidth, getStage().getCamera().viewportHeight));
	}

	public void setCamPos(float x, float y) {
		getStage().getCamera().position.set(x,y,0);
	}

	public Vector3 getCamPos() {
		return(getStage().getCamera().position);
	}

	public Stage getStage() {
		return stage;
	}

	public void setStage(Stage stage) {
		this.stage = stage;
	}

	public float getScale() {
		float f = getStage().getCamera().viewportWidth;
		return f/Gdx.graphics.getWidth();
	}

	public Camera getCam() {
		return getStage().getCamera();
	}
}
