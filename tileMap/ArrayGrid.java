package engine.tileMap;

import com.badlogic.gdx.utils.Array;

public class ArrayGrid<T> implements Grid<T>{
	private Array<Array<T>> mData;
	private int mW , mH;

	public ArrayGrid(int w,int h){
		mW = w;
		mH = h;
		resetData();

	}

	private void resetData() {
		mData = new Array<Array<T>>(mH);
		for(int i = 0;i < mH;i++){
			mData.add(new Array<T>(mW));
		}
	}

	@Override
	public T get(int i, int j) {
		return null;
	}

	@Override
	public int getWidth() {
		return mW;
	}

	@Override
	public int getHeight() {
		return mH;
	}
}
