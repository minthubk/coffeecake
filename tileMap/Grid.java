package engine.tileMap;

public interface Grid<T> {

	public T get(int i,int j);
	public int getWidth();
	public int getHeight();
}
