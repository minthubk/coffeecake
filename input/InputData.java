package engine.input;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;

import engine.InputController.InputType;
import engine.gui.ControlGUI;
import engine.input.KeyInputProcessor.Action;

public class InputData{
		public ObjectMap<Action, Integer> binds;
		public Array<Action> actions = new Array<Action>();
		public InputType type;
		public int playerId;
		public boolean mouseRot;
		public boolean lockRot;
		
		public InputData(int id,InputType t,ObjectMap<Action, Integer> bind) {
			this.binds = bind;
			for (Action a  : Action.values()) {
				if(bind.containsKey(a))
					actions.add(a);
			}
			this.type = t;
			this.playerId = id;
		}

		public InputData(int id,InputType t) {
			this.binds = ControlGUI.getBind(t);
			for (Action a  : Action.values()) {
				if(binds.containsKey(a))
					actions.add(a);
			}
			this.type = t;
			this.playerId = id;
		}

		public void clearBinds(){
			for (Action a : binds.keys()) {
				binds.put(a, -1);
			}
		}

		public boolean bindKey(Action a,Integer k){
			if(!binds.containsValue(k, false)){
				binds.put(a, k);
				return true;
			}
			return false;
		}

		public Action getAction(int focus) {
			if(actions.size <= focus)
				return null;
			return actions.get(focus);
		}

	}
