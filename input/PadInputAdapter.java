package engine.input;



import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerAdapter;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.ObjectMap;

import engine.GameScreen;
import engine.input.KeyInputProcessor.Action;
import engine.message.InputMessage;

public class PadInputAdapter extends ControllerAdapter {

	GameScreen screen;
	int mTargetEntityId = -1;
	public ObjectMap<Action, Integer> bind = new ObjectMap<Action, Integer>();
	private float currentY;
	private float currentX;
	boolean A = false;
	boolean B = false;

	public void setBind(ObjectMap<Action, Integer> bind) {
		this.bind = bind;
	}
	private Controller mController;

	public Controller getmController() {
		return mController;
	}

	public int getmTargetEntityId() {
		return mTargetEntityId;
	}

	public void setId(int id) {
		this.mTargetEntityId = id;
	}

	public PadInputAdapter(GameScreen screen, Controller controller) {
		this.screen = screen;
		this.mController = controller;
		bind.put(Action.Fire,0);
		bind.put(Action.Fire2,1);

	}

	public PadInputAdapter(GameScreen screen, Controller controller,ObjectMap<Action, Integer> bind) {
		this.screen = screen;
		this.mController = controller;
		this.bind = bind;
	}

	private void sendMessage(Vector2 xy) {
		screen.sendMessage(new InputMessage(mTargetEntityId,xy,xy.cpy(), A, B));
	}

	public void connected(Controller controller)
	{

	}

	public void disconnected(Controller controller){
		if(mController == controller){

		}
	}

	public boolean buttonDown (Controller controller, int buttonCode){
		if(mController == controller){
			if(buttonCode == bind.get(Action.Fire)){
				A = true;
				sendMessage(new Vector2(currentX,currentY));
			}
			if(buttonCode == bind.get(Action.Fire2)){
				B = true;
				sendMessage(new Vector2(currentX,currentY));
			}

			return true;
		}
		return false;
	}

	public boolean buttonUp (Controller controller, int buttonCode){
		if(mController == controller){
			if(buttonCode == bind.get(Action.Fire)){
				A = false;
				sendMessage(new Vector2(currentX,currentY));
			}
			if(buttonCode == bind.get(Action.Fire2)){
				B = false;
				sendMessage(new Vector2(currentX,currentY));
			}

			return true;
		}
		return false;
	}

	public boolean axisMoved (Controller controller, int axisCode, float value)
	{		
		return false;
	}

	public boolean povMoved (Controller controller, int povCode, PovDirection value){
		if(mController == controller){
			float x=0, y=0;
			y = -currentY;
			x = -currentX;
			sendMessage(new Vector2(x,y));
			currentX= 0;
			currentY = 0;
			x = 0;
			y = 0;
			if(value == PovDirection.north || value == PovDirection.northEast ||value == PovDirection.northWest){

				y += 1;
			}

			if(value == PovDirection.south || value == PovDirection.southEast ||value == PovDirection.southWest){
				y -= 1;
			}

			if(value == PovDirection.east || value == PovDirection.northEast ||value == PovDirection.southEast){
				x += 1;
			}

			if(value == PovDirection.west|| value == PovDirection.northWest||value == PovDirection.southWest){
				x -= 1;
			}
			sendMessage(new Vector2(x,y));
			currentX = x;
			currentY = y;

			return true;
		}
		return false;
	}

	public boolean xSliderMoved (Controller controller, int sliderCode, boolean value){
		//sendMessage(new Vector2( x, 0), false, false);
		return true;
	}

	public boolean ySliderMoved (Controller controller, int sliderCode, boolean value){

		//sendMessage(new Vector2( 0, y), false, false);
		return true;
	}

	public boolean accelerometerMoved (Controller controller, int accelerometerCode, Vector3 value){
		return false;
	}

	public ObjectMap<Action, Integer> getBind() {
		return bind;
	}
}
