package engine.input;

import engine.message.BaseMessage;
import engine.message.MessageType;

public class TouchFireMsg extends BaseMessage {

	
	public boolean touched;

	public TouchFireMsg(boolean b) {
		super(MessageType.touchFire);
		this.touched = b;
	}

}
