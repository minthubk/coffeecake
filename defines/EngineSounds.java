package engine.defines;

public class EngineSounds {
	public static final String soundDir = "data/snd/";
	public static final String musicDir = "data/music/";
	
	public static final String button = "button.wav";
	public static final String newPlayer = "newPlayer.wav";

	public static String[] sounds = {button, newPlayer};
}
