package engine.defines;

public class EngineValues {
		public enum lang{en,fr};
		public static lang language = lang.en;
		
		public static final float bodyScale = 10;
		public static final float GUIIconSize = 32;

		/*Graphics*/
		public static int screenw = 700;
		public static int screenh = 600;
		public static float viewportRatio = 2f; //Ratio for android
		public static float viewportW = 400f;
		public static float viewportH = 400f;

		public static final String font = "data/font/default.fnt";
		public static final String classicFont = "data/font/classic.fnt";

		//Should be false	
		public static boolean box2DDebug = false;

		public static boolean KongregateAPI = false;

		public static boolean mobileDebug = true;

		public static boolean adDebug = false;
		public static final boolean fullScreenDebug = true;
		public static final boolean texturePackingDebug = true;

		public static final int menuH = 80;

		public static final String keyboardText = "Press [SPACE] to use keyboard";

		public static final String numpadText = "Press [0] to use num pad";

		public static final String texturesPath = "texture";

}
