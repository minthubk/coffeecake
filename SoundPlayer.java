package engine;

import java.util.HashMap;

import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;

import engine.message.BaseMessage;
import engine.message.MessageType;
import engine.message.PlaySound;
import engine.message.PlaySound.Order;

public class SoundPlayer {
	
	public static final String soundDir = "data/snd/";
	public static final String musicDir = "data/music/";
	HashMap<String, Sound> loops = new HashMap<String, Sound>(); // looops array
	protected SimulationRenderer mSimRenderer;
	protected Music mCurrentMusic;
	public static float volume = 0.5f;
	
	public SoundPlayer(SimulationRenderer simRenderer) {
		mSimRenderer = simRenderer;
	}
	
	protected Sound getSound(String soundName) {
		soundName = soundDir + soundName;
		Sound s = mSimRenderer.mAssets.getSound(soundName);
		return s;
	}

	protected Music getMusic(String musicName) {
		musicName = musicDir + musicName;
		Music m = mSimRenderer.mAssets.getMusic(musicName);
		return m;
	}

	protected String getHash(int srcId, String sound) {
		return sound+srcId;
	}
	
	/*
	private void add(Sound s) {
		sounds.insert(0, s);
		if(sounds.size > 20)
			sounds.pop().dispose();
	}*/

	void playSound(String sound){
		Sound s = getSound(sound);
		if(s!= null)
			s.play(volume);
	}

	void loopSound(String soundName, int srcId){
		Sound s = getSound(soundName);
		
		if(s != null) {
			s.loop(volume);
			loops.put(getHash(srcId, soundName), s);
		}
	}
	
	void playMusic(String musicName){
		if(mCurrentMusic != null)
			mCurrentMusic.dispose();
		
		mCurrentMusic = getMusic(musicName);
		
		if(mCurrentMusic != null)
			mCurrentMusic.play();
	}
	
	void loopMusic(String musicName){
		playMusic(musicName);
		
		if(mCurrentMusic != null)
			mCurrentMusic.setLooping(true);
	}
	
	void stop(int srcId, String sound) {	
		String k = getHash(srcId, sound); 
			Sound s = loops.get(k);
			if(s != null)
				s.stop();
	}
	
	void stopMusic() {	
		if(mCurrentMusic != null) {
			mCurrentMusic.stop();
			mCurrentMusic.dispose();
		}
	}

	public boolean sendMessage(BaseMessage msg) {
		if(msg.messageType == MessageType.Sound) {
			PlaySound m = (PlaySound)msg;
			if(m.mOrder == Order.volume){
				volume = m.getmVolume();
			}
			if(m.mOrder == Order.play && m.mIsMusic && m.mIsLoop == false)
				playMusic(m.mSource);
			if(m.mOrder == Order.play && m.mIsMusic && m.mIsLoop == true)
				loopMusic(m.mSource);
			
			else if(m.mOrder == Order.stop && m.mIsMusic)
				stopMusic();
			
			else if(m.mOrder == Order.play && m.mIsMusic == false && m.mIsLoop == false)
				playSound(m.mSource);
			else if(m.mOrder == Order.play && m.mIsMusic == false && m.mIsLoop == true)
				loopSound(m.mSource, m.mEmitter);
			
			else if(m.mOrder == Order.stop && m.mIsMusic == false)
				stop(m.mEmitter, m.mSource);
			return true;
		}
		return false;
	}
	
}
