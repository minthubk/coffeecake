package engine.entity;

import com.badlogic.gdx.math.Vector2;

import engine.behavior.TextureRenderer;
import engine.behavior.tools.Temporary;

public class Particle extends Entity{

	protected float ttl;
	protected Temporary tmp;
	protected float scale = 1;
	protected TextureRenderer renderer;
	
	public Particle(float s, float ttl) {
		this.scale = s;
		this.ttl = ttl;
	}
	
	public Particle(float ttl) {
		scale = 1;
		this.ttl = ttl;
	}

	@Override
	protected void initFixture() {
		
	}
	
	@Override
	public void destroy() {
		mSimController.removeParticle(getId());
	}
	
	public void setTTL(float ttl){
		this.ttl = ttl;
		if(tmp != null)
			tmp.setTimer(ttl);
		else
			tmp = (Temporary)createBehavior(new Temporary(ttl));
	}
	
	@Override
	protected void initActions() {
		super.initActions();
		renderer = (TextureRenderer) setRenderer(new TextureRenderer());
		renderer.setmScale(new Vector2(scale,scale));
		if(ttl > 0)
			tmp = (Temporary)createBehavior(new Temporary(ttl));
	}

}
